from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required, user_passes_test
from .models import Program, Module, Submission

import lec
# Create your views here.
def is_student(user):
    return user.is_student

def login_redirect(request):
    if request.user.is_authenticated:    
        if request.user.is_student:
            return redirect("student")
        elif request.user.is_lecturer:
            return redirect(lec.views.lecture)
        elif request.user.is_staff:
            return redirect("admin")
       

@login_required(login_url='/accounts/login/')
@user_passes_test(is_student)
def student(request):
    return render(request, 'student.html')

def program(request, program_id):
    program = Program.objects.get(id=program_id)
