# Generated by Django 2.0.2 on 2018-03-12 20:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lec', '0005_assessment_project'),
        ('student', '0003_module_units'),
    ]

    operations = [
        migrations.CreateModel(
            name='Submission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=140)),
                ('link', models.CharField(blank=True, max_length=140)),
                ('upload_file', models.FileField(upload_to='')),
                ('additional_notes', models.TextField(max_length=500)),
                ('project', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='project', to='lec.Project')),
                ('student_user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='student', to='student.Student')),
            ],
        ),
    ]
