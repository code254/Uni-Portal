from django.conf.urls import url, include
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    # lec dashboards
    url(r'^$', views.student, name='student'),
    url(r'^login_redirect$', views.login_redirect, name='login_redirect')

]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)