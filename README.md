# Uni-Portal 101

A Web application that seeks to make campus life easier by enhancing communication between lecturers and students

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
```
astroid==1.6.1
Django==2.0.2
django-bootstrap3==9.1.0
isort==4.3.4
lazy-object-proxy==1.3.1
mccabe==0.6.1
Pillow==5.0.0
psycopg2==2.7.4
pylint==1.8.2
python-decouple==3.1
pytz==2018.3
six==1.11.0
wrapt==1.10.11
```

### Installing

A step by step series of examples that tell you have to get a development env running

Clone the repo

```
git clone https://gitlab.com/code254/Uni-Portal.git
```
Create branch

```
git branch branch_name
``` 

Switch to the branch you just created 

```
git checkout branch_name
```

Create the virtual environment

```
virtualenv virtual
```

Activate the virtual environment 

```
source virtual/bin/activate
```

Install all pre-requisites 

```
pip install -r requirements.txt
```
Now you are good to go

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [django](https://www.djangoproject.com/) - the web framework used
* [python](https://www.python.org/) - the main programming language
* [PostgreSQL](https://www.postgresql.org/) - the database used 

## Authors

* **Virginia Ndung'u** 
* **Mweru Muchai** 
* **Kevin** 

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details



